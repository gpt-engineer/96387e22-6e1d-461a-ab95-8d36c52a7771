// Game variables
var canvas, ctx;
var box = 32;
var ground = [];
var snake = [];
snake[0] = { x: 9 * box, y: 10 * box };
var food = { x: Math.floor(Math.random() * 17 + 1) * box, y: Math.floor(Math.random() * 15 + 3) * box };
var score = 0;
var d;

// Load assets
var ground = new Image();
ground.src = "img/ground.png";
var foodImg = new Image();
foodImg.src = "img/food.png";

// Create the canvas
function createCanvas() {
  canvas = document.getElementById("game");
  ctx = canvas.getContext("2d");
}

// Draw everything onto the canvas
function draw() {
  ctx.drawImage(ground, 0, 0);
  for (var i = 0; i < snake.length; i++) {
    ctx.fillStyle = (i == 0) ? "green" : "white";
    ctx.fillRect(snake[i].x, snake[i].y, box, box);
    ctx.strokeStyle = "red";
    ctx.strokeRect(snake[i].x, snake[i].y, box, box);
  }
  ctx.drawImage(foodImg, food.x, food.y);
  ctx.fillStyle = "white";
  ctx.font = "45px Changa one";
  ctx.fillText(score, 2 * box, 1.6 * box);
}

// Update the game state
function update() {
  var snakeX = snake[0].x;
  var snakeY = snake[0].y;

  // which direction
  if( d == "LEFT") snakeX -= box;
  if( d == "UP") snakeY -= box;
  if( d == "RIGHT") snakeX += box;
  if( d == "DOWN") snakeY += box;

  // If the snake eats the food
  if(snakeX == food.x && snakeY == food.y){
      score++;
      food = {
          x : Math.floor(Math.random()*17+1) * box,
          y : Math.floor(Math.random()*15+3) * box
      }
      // we don't remove the tail
  }else{
      // remove the tail
      snake.pop();
  }
  
  // add new Head
  
  var newHead = {
      x : snakeX,
      y : snakeY
  }
  
  // game over
  
  if(snakeX < box || snakeY < 3*box || snakeX > 17 * box || snakeY > 17*box || collision(newHead,snake)){
      clearInterval(game);
  }
  
  snake.unshift(newHead);
}

// Handle user input
function direction(event) {
  let key = event.keyCode;
  if( key == 37 && d != "RIGHT"){
      d = "LEFT";
  }else if(key == 38 && d != "DOWN"){
      d = "UP";
  }else if(key == 39 && d != "LEFT"){
      d = "RIGHT";
  }else if(key == 40 && d != "UP"){
      d = "DOWN";
  }
}

// Run the game
function runGame() {
  createCanvas();
  setInterval(draw,100);
  setInterval(update,100);
}

// Collision check
function collision(head,array){
  for(let i = 0; i < array.length; i++){
      if(head.x == array[i].x && head.y == array[i].y){
          return true;
      }
  }
  return false;
}

// Start the game
document.addEventListener("keydown",direction);
runGame();